<?php


namespace Centralpos\BinaryUuid;

use Illuminate\Support\Str;
use Ramsey\Uuid\Uuid;

trait BinaryUuid
{

    public static function bootBinaryUuid()
    {
        static::creating(function ($model) {

            $model->{$model->getKeyName()} = $model->{$model->getKeyName()} ?: Str::orderedUuid()->toString();
        });
    }

    /**
     * @param  string $key
     * @param  mixed $value
     * @return bool|\Illuminate\Support\Collection|int|mixed|string|null
     */
    protected function castAttribute($key, $value)
    {
        if ($this->getCastType($key) != 'uuid' || is_null($value)) {
            return parent::castAttribute($key, $value);
        }

        if (Uuid::isValid($value)) {
            return $value;
        }

        if ($this->isSqlSrvDriver()) {
            $value = $this->sortSqlSrvGuid($value);
        }

        return Uuid::fromBytes($value)->toString();
    }

    /**
     * @param $value
     */
    protected function sortSqlSrvGuid($value)
    {
        $data = bin2hex($value);

        $first = hex2bin(
            strrev(substr($data, 0, 8)) .
            strrev(substr($data, 8, 4)) .
            strrev(substr($data, 12, 4))
        );

        $first = pack('H*', implode('', unpack('h*', $first)));

        $last = hex2bin(substr($data, 16, 16));

        return $first.$last;
    }

    /**
     * Set a given attribute on the model.
     *
     * @param  string  $key
     * @param  mixed  $value
     * @return mixed
     */
    public function setAttribute($key, $value)
    {
        if ($this->hasCast($key, ['uuid']) && $this->convertStrings()) {
            $value = Uuid::fromString($value)->getBytes();
        }

        return parent::setAttribute($key, $value);
    }

    /**
     * Get the casts array.
     *
     * @return array
     */
    public function getCasts()
    {
        return array_merge([$this->getKeyName() => $this->getKeyType()], $this->casts);
    }

    /**
     * Get the auto-incrementing key type.
     *
     * @return string
     */
    public function getKeyType()
    {
        return 'uuid';
    }

    /**
     * @return false
     */
    public function getIncrementing()
    {
        return false;
    }

    /**
     * @return QueryBuilder|\Illuminate\Database\Query\Builder
     */
    protected function newBaseQueryBuilder()
    {
        $uuidColumns = [];

        if ($this->convertStrings()) {
            foreach ($this->getCasts() as $column => $type) {
                if ($type == 'uuid') {
                    $uuidColumns[] = $column;
                }
            }
        }

        $conn = $this->getConnection();

        $grammar = $conn->getQueryGrammar();

        return new QueryBuilder($conn, $grammar, $conn->getPostProcessor(), $uuidColumns);
    }

    /**
     * @return bool
     */
    protected function convertStrings()
    {
        return !$this->inputsShouldBeStrings();
    }

    /**
     * @return bool
     */
    protected function isSqlSrvDriver()
    {
        return $this->getConnection()->getDriverName() == 'sqlsrv';
    }

    /**
     * @return mixed
     */
    protected function getKeyForSaveQuery()
    {
        $key = $this->getKeyName();
        $value = parent::getKeyForSaveQuery();

        if (!$this->hasCast($key, ['uuid']) || is_null($value)) {
            return $value;
        }

        if ($this->inputsShouldBeStrings() && !Uuid::isValid($value)) {
            $value = $this->castAttribute($key, $value);
        }

        return $value;
    }

    /**
     * @return bool
     */
    protected function inputsShouldBeStrings()
    {
        $driver = $this->getConnection()->getDriverName();

        switch ($driver) {
            case 'mysql':
                return false;
        }

        return true;
    }
}
