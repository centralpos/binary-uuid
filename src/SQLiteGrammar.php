<?php


namespace Centralpos\BinaryUuid;


use Illuminate\Support\Fluent;

class SQLiteGrammar extends \Illuminate\Database\Schema\Grammars\SQLiteGrammar
{
    protected function typeBinaryUuid(Fluent $column)
    {
        return 'varchar';
    }
}
