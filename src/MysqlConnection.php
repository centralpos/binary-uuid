<?php


namespace Centralpos\BinaryUuid;


class MysqlConnection extends \Illuminate\Database\MySqlConnection
{
    protected function getDefaultSchemaGrammar()
    {
        return $this->withTablePrefix(new MysqlGrammar);
    }
}
