<?php


namespace Centralpos\BinaryUuid;


class SqlServerConnection extends \Illuminate\Database\SqlServerConnection
{
    protected function getDefaultSchemaGrammar()
    {
        return $this->withTablePrefix(new SqlServerGrammar());
    }
}
