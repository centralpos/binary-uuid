<?php


namespace Centralpos\BinaryUuid;


class SQLiteConnection extends \Illuminate\Database\SQLiteConnection
{
    protected function getDefaultSchemaGrammar()
    {
        return $this->withTablePrefix(new SQLiteGrammar());
    }
}
