<?php


namespace Centralpos\BinaryUuid;


use Illuminate\Support\Fluent;

class SqlServerGrammar extends \Illuminate\Database\Schema\Grammars\SqlServerGrammar
{
    protected function typeBinaryUuid(Fluent $column)
    {
        return 'uniqueidentifier';
    }
}
