<?php


namespace Centralpos\BinaryUuid;

use Illuminate\Contracts\Support\Arrayable;
use Illuminate\Database\ConnectionInterface;
use Illuminate\Database\Query\Builder;
use Illuminate\Database\Query\Grammars\Grammar;
use Illuminate\Database\Query\Processors\Processor;
use Illuminate\Support\Str;
use Ramsey\Uuid\Uuid;

class QueryBuilder extends Builder
{
    /**
     * @var array
     */
    public $uuidColumns;

    /**
     * QueryBuilder constructor.
     * @param  ConnectionInterface $connection
     * @param  Grammar|null $grammar
     * @param  Processor|null $processor
     * @param  array $uuidColumns
     */
    public function __construct(ConnectionInterface $connection, Grammar $grammar = null, Processor $processor = null, array $uuidColumns)
    {
        parent::__construct($connection, $grammar, $processor);

        $this->uuidColumns = $uuidColumns;
    }

    /**
     * @param  array|\Closure|string $column
     * @param  null $operator
     * @param  null $value
     * @param  string $boolean
     * @return QueryBuilder
     */
    public function where($column, $operator = null, $value = null, $boolean = 'and')
    {
        list($column, $operator, $value) = $this->resolveValue($column, $operator, $value);

        return parent::where($column, $operator, $value, $boolean);
    }

    /**
     * @param  mixed $column
     * @param  mixed $operator
     * @param  mixed $value
     * @return array
     */
    protected function resolveValue($column, $operator, $value)
    {
        if ($this->columnIsUuid($column)) {

            $bytes = $this->uuidToBytes($value ?: $operator);

            if ($value !== null) {
                $value = $bytes;
            } else {
                $operator = $bytes;
            }
        }

        return [$column, $operator, $value];
    }

    /**
     * @param  string $column
     * @param  mixed $values
     * @param  string $boolean
     * @param  false $not
     * @return QueryBuilder
     */
    public function whereIn($column, $values, $boolean = 'and', $not = false)
    {
        $values = $this->columnIsUuid($column) ? $this->uuidToBytes($values) : $values;

        return parent::whereIn($column, $values, $boolean, $not);
    }

    /**
     * @param  array|\Closure|string $column
     * @param  null $operator
     * @param  null $value
     * @return QueryBuilder
     */
    public function orWhere($column, $operator = null, $value = null)
    {
        list($column, $operator, $value) = $this->resolveValue($column, $operator, $value);

        return parent::orWhere($column, $operator, $value);
    }

    /**
     * Remove the table name from a given key.
     *
     * @param  string  $column
     * @return string
     */
    protected function removeTableFromColumn(string $column)
    {
        return Str::contains($column, '.')
            ? last(explode('.', $column)) : $column;
    }

    /**
     * @param  string $column
     * @return bool
     */
    protected function columnIsUuid($column)
    {
        if (is_string($column)) {
            return in_array($this->removeTableFromColumn($column), $this->uuidColumns) ;
        }

        return false;
    }

    /**
     * @param  string $uuid
     * @return string|array
     */
    public function uuidToBytes($uuid)
    {
        if (is_array($uuid) || $uuid instanceof Arrayable) {
            return $this->uuidsArrayToBytes($uuid);
        } elseif (is_string($uuid) && Uuid::isValid($uuid)) {
            return Uuid::fromString($uuid)->getBytes();
        }

        return $uuid;
    }

    /**
     * @param  array $uuids
     * @return array|array[]|string[]
     */
    public function uuidsArrayToBytes($uuids)
    {
        if ($uuids instanceof Arrayable) {
            $uuids = $uuids->toArray();
        }

        return array_map(function ($uuid) {
            return $this->uuidToBytes($uuid);
        }, $uuids);
    }
}
