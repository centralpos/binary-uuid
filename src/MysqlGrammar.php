<?php


namespace Centralpos\BinaryUuid;


use Illuminate\Support\Fluent;

class MysqlGrammar extends \Illuminate\Database\Schema\Grammars\MySqlGrammar
{
    protected function typeBinaryUuid(Fluent $column)
    {
        return 'binary(16)';
    }
}
